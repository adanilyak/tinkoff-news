//
//  ScreensAssembler.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Assembler for building all possible application screens
class ScreensAssembler {
    
    //MARK: Services
    
    private(set) lazy var apiServiceConfiguration: APIServiceConfiguration
        = APIServiceConfiguration(scheme: "https", host: "api.tinkoff.ru")
    
    private(set) lazy var apiService: APIService
        = APIService(serviceConfiguration: self.apiServiceConfiguration)
    
    private(set) lazy var coreDataService: CDService = CDService()
    
    //MARK: Facades
    
    private(set) lazy var newsFacade: NewsFacade = NewsFacade(api: self.apiService,
                                                              coreData: self.coreDataService)
    
}

// MARK: - ScreensAssemblerProtocol
extension ScreensAssembler: ScreensAssemblerProtocol {}

// MARK: - NewsModuleAssemblerProtocol
extension ScreensAssembler: NewsModuleAssemblerProtocol {
    
    func assembleNewsHeadersListViewController(router: NewsModuleRouterProtocol) -> UIViewController {
        let vmConfig = NewsHeadersListViewModelInitConfig(newsFacade: newsFacade,
                                                          router: router)
        let vm = NewsHeadersListViewModel(config: vmConfig)
        let vc = NewsHeadersListViewController(vm)
        return vc
    }
    
    func assembleNewsContentViewController(with content: NewsContent) -> UIViewController {
        let vmConfig = NewsContentViewModelInitConfig(content: content)
        let vm = NewsContentViewModel(config: vmConfig)
        let vc = NewsContentViewController(vm)
        return vc
    }
    
}
