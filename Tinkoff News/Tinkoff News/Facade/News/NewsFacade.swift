//
//  NewsFacade.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreData

/// Facade for loading news
class NewsFacade {
    
    /// API Service
    /// Use it to ask server for data
    let api: APIServiceProtocol
    
    /// Core Data Service
    /// Use it to all requests for DB
    let coreData: CDServiceProtocol
    
    init(api: APIServiceProtocol, coreData: CDServiceProtocol) {
        self.api = api
        self.coreData = coreData
    }
    
}

// MARK: - Load News
extension NewsFacade {
    
    /// Alias for generic completion
    typealias Completion<Value> = (Result<Value>) -> Void
    
    /// Load news header from server with pagination
    /// and store the result in DB
    ///
    /// - Parameters:
    ///   - offset: start index
    ///   - pageSize: amount to load
    ///   - completion: completion (loaded headers, is news headers intersect with some of alredy loaded)
    func loadNewsHeaders(for offset: UInt, pageSize: UInt, completion: Completion<([NewsHeader], Bool)>?) {
        api.getNewsHeaders(for: offset, pageSize: pageSize) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .error(let error):
                completion?(.error(APIServiceError.response(error)))
            case .success(let json):
                guard let headersJsons: [JSON] = json["payload"] as? [JSON] else {
                    completion?(.error(APIServiceError.serialization))
                    return
                }
                
                let context = strongSelf.coreData.persistentContainer.viewContext
                context.perform {
                    // Checking DB for intersections with incoming headers
                    let incomingIds: [Int64] = headersJsons.compactMap { Int64(with: $0["id"] as? String) }
                    let hasIntersections = strongSelf.coreData.isNewsHeaderExistWithId(in: incomingIds, context: context)
                    
                    // Creating / Updating headers in DB
                    let headers = headersJsons.compactMap { strongSelf.coreData.createNewsHeader(from: $0, context: context) }
                    strongSelf.coreData.save(context: context)
                    completion?(.success((headers, hasIntersections)))
                }
            }
        }
    }
    
    /// Load news content from server for header
    ///
    /// - Parameters:
    ///   - header: header of content to load
    ///   - completion: completion
    func loadNewsContent(for header: NewsHeader, completion: Completion<NewsContent>?) {
        api.getNewsContent(for: Int(header.id)) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .error(let error):
                completion?(.error(APIServiceError.response(error)))
            case .success(let json):
                guard let contentJson: JSON = json["payload"] as? JSON else {
                    completion?(.error(APIServiceError.serialization))
                    return
                }
                
                let context = strongSelf.coreData.persistentContainer.viewContext
                context.perform {
                    guard let content = strongSelf.coreData.createNewsContent(from: contentJson, context: context) else {
                        completion?(.error(CDServiceError.objectNotCreated))
                        return
                    }
                    
                    header.content = content
                    strongSelf.coreData.save(context: context)
                    completion?(.success(content))
                }
            }
        }
    }
    
    /// Increase views count of header by 1 and save to DB
    ///
    /// - Parameter header: header
    func increaseViewsCount(for header: NewsHeader) {
        header.numberOfViews += 1
        let context = coreData.persistentContainer.viewContext
        context.perform { [weak self] in
            self?.coreData.save(context: context)
        }
    }
    
}
