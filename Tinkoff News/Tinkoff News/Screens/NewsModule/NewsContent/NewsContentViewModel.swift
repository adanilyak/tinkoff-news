//
//  NewsContentViewModel.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 04/06/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Config for initing VM
struct NewsContentViewModelInitConfig {
    let content: NewsContent
}

/// VM for content view controller
class NewsContentViewModel {
    
    /// Content to display
    private let content: NewsContent
    
    /// Shortcut getter for HTML string
    var htmlString: String {
        return content.content ?? tr(key: "news_content.no_content")
    }
    
    /// Title for view controller
    var title: String? {
        return content.header?.text
    }
    
    init(config: NewsContentViewModelInitConfig) {
        self.content = config.content
    }
    
}
