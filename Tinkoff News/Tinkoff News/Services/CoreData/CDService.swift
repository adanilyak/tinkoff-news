//
//  CDService.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreData

/// Enity names used in CD
///
/// - newsHeader: header
/// - newsContent: content
enum CDEntityName: String {
    case newsHeader = "NewsHeader"
    case newsContent = "NewsContent"
}

/// CD Errors
///
/// - notFound: object has not been found in CD
/// - objectNotCreated: can not create object
enum CDServiceError: Error {
    case notFound
    case objectNotCreated
}

/// Implementation of CD service protocol
class CDService: CDServiceProtocol {
    
    /// Container
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Tinkoff_News")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                assert(false, "Unresolved error \(error), \(error.userInfo)")
            }
        })
        // Incoming changes have priority over stored values
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return container
    }()
    
    /// Save context
    ///
    /// - Parameter context: context
    func save(context: NSManagedObjectContext) {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                assert(false, "Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
