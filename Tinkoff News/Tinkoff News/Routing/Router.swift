//
//  Router.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 30/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Main application router
class Router: RouterProtocol {
    
    /// Assembler for building all possible application screens
    let assembler: ScreensAssemblerProtocol
    
    /// Root navigation controller
    let rootController: UINavigationController = UINavigationController()
    
    init(with assembler: ScreensAssemblerProtocol) {
        self.assembler = assembler
    }
    
}

extension Router: NewsModuleRouterProtocol {
    
    func routeToNewsHeadersList() {
        let vc = assembler.assembleNewsHeadersListViewController(router: self)
        rootController.pushViewController(vc, animated: true)
    }
    
    func routeToNewsContent(with content: NewsContent) {
        let vc = assembler.assembleNewsContentViewController(with: content)
        rootController.pushViewController(vc, animated: true)
    }
    
}
