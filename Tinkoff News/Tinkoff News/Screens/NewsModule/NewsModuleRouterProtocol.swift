//
//  NewsModuleRouterProtocol.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 29/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Router protocol needed for News User Story
protocol NewsModuleRouterProtocol {
    
    /// Route to header list VC
    func routeToNewsHeadersList()
    
    /// Route to content VC
    ///
    /// - Parameter content: content
    func routeToNewsContent(with content: NewsContent)
    
}
