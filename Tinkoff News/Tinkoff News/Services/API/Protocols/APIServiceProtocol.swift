//
//  APIServiceProtocol.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 27/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Result of any operation with generic success value
///
/// - success: success
/// - error: failure
enum Result<Value> {
    case success(Value)
    case error(Error)
}

/// Alias for result of API Call
typealias APICompletion<Value> = (Result<Value>) -> Void

/// Common JSON alias
typealias JSON = [String: Any]

protocol APIServiceProtocol: APIServiceProtocolNews {}
