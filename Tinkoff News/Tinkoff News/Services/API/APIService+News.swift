//
//  APIService+News.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

// MARK: - APIServiceProtocolNews
extension APIService: APIServiceProtocolNews {
    
    func getNewsHeaders(for offset: UInt, pageSize: UInt, completion: APICompletion<JSON>?) {
        let queryParams: QueryParams = ["first": "\(offset)", "last": "\(pageSize + offset)"]
        guard let url = createURL(pathComponents: ["v1", "news"], queryParams: queryParams) else {
            completion?(Result.error(APIServiceError.invalidURL))
            return
        }
        
        let request: URLRequest = createRequest(with: url, method: .get)
        
        let dataTask: URLSessionDataTask = urlSession.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion?(Result.error(APIServiceError.response(error)))
                return
            }
            
            guard let data: Data = data else {
                completion?(Result.error(APIServiceError.dataExpected))
                return
            }
            
            guard let jsonObject: Any = try? JSONSerialization.jsonObject(with: data, options: []),
                let json: JSON = jsonObject as? JSON else {
                    completion?(Result.error(APIServiceError.serialization))
                    return
            }
            
            completion?(Result.success(json))
        }
        
        dataTask.resume()
    }
    
    func getNewsContent(for id: Int, completion: APICompletion<JSON>?) {
        let queryParams: QueryParams = ["id": "\(id)"]
        guard let url = createURL(pathComponents: ["v1", "news_content"], queryParams: queryParams) else {
            completion?(Result.error(APIServiceError.invalidURL))
            return
        }
        
        let request: URLRequest = createRequest(with: url, method: .get)
        
        let dataTask: URLSessionDataTask = urlSession.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion?(Result.error(APIServiceError.response(error)))
                return
            }
            
            guard let data: Data = data else {
                completion?(Result.error(APIServiceError.dataExpected))
                return
            }
            
            guard let jsonObject: Any = try? JSONSerialization.jsonObject(with: data, options: []),
                let json: JSON = jsonObject as? JSON else {
                    completion?(Result.error(APIServiceError.serialization))
                    return
            }
            
            completion?(Result.success(json))
        }
        
        dataTask.resume()
    }
    
}
