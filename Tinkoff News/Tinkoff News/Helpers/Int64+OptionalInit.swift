//
//  Int64+OptionalInit.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 06/06/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

extension Int64 {
    
    /// Failable init for Int64 from optional string
    ///
    /// - Parameter string: string
    init?(with string: String?) {
        guard let string = string else {
            return nil
        }
        
        self.init(string)
    }
    
}
