//
//  RouterProtocol.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 30/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Protocol for incapsulating in-app navigation
protocol RouterProtocol: class, NewsModuleRouterProtocol {
    
    /// Base root controller
    var rootController: UINavigationController { get }
    
}
