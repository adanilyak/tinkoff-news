//
//  APIService.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 27/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// API Service configuration
struct APIServiceConfiguration {
    let scheme: String
    let host: String
    
    init(scheme: String, host: String) {
        self.scheme = scheme
        self.host = host
    }
    
}

/// API Errors
///
/// - invalidURL: invalid url
/// - response: server responded with error
/// - dataExpected: no incoming data
/// - serialization: wrong json
enum APIServiceError: Error {
    case invalidURL
    case response(Error)
    case dataExpected
    case serialization
}

/// Implementation of API Service protocol
class APIService: APIServiceProtocol {
    
    /// Configuration
    let serviceConfiguration: APIServiceConfiguration
    
    /// Session
    let urlSession: URLSession = {
        let configuration: URLSessionConfiguration = URLSessionConfiguration.default
        return URLSession(configuration: configuration)
    }()
    
    init(serviceConfiguration: APIServiceConfiguration) {
        self.serviceConfiguration = serviceConfiguration
    }
    
}

extension APIService {
    
    //MARK: Aliases
    
    typealias QueryParams = [String: String]
    
    //MARK: URL Builder
    
    func createURL(pathComponents: [String], queryParams: QueryParams = [:]) -> URL? {
        var urlComponents: URLComponents = URLComponents()
        urlComponents.scheme = serviceConfiguration.scheme
        urlComponents.host = serviceConfiguration.host
        
        if !pathComponents.isEmpty {
            urlComponents.path = "/" + pathComponents.joined(separator: "/")
        }
        
        if !queryParams.isEmpty {
            urlComponents.queryItems = queryParams.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        
        return urlComponents.url
    }
    
    //MARK: Request Builder
    
    enum Method: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }
    
    func createRequest(with url: URL, method: Method, params: JSON? = nil) -> URLRequest {
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = method.rawValue
        if let params: JSON = params {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }
        
        return request
    }
    
}
