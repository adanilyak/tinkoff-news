//
//  CDServiceProtocolNews.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreData

/// CD protocol required for News User Story
protocol CDServiceProtocolNews {
    
    /// Create header if it is not exist, update in the other case
    /// Using json
    ///
    /// - Parameters:
    ///   - json: json
    ///   - context: context
    /// - Returns: header
    func createNewsHeader(from json: JSON, context: NSManagedObjectContext) -> NewsHeader?
    
    /// Create content using json
    ///
    /// - Parameters:
    ///   - json: json
    ///   - context: context
    /// - Returns: content
    func createNewsContent(from json: JSON, context: NSManagedObjectContext) -> NewsContent?
    
    /// Check if any header with id from the array exist in CD
    ///
    /// - Parameters:
    ///   - ids: array of ids
    ///   - context: context
    /// - Returns: exist / not exist
    func isNewsHeaderExistWithId(in ids: [Int64], context: NSManagedObjectContext) -> Bool
    
}
