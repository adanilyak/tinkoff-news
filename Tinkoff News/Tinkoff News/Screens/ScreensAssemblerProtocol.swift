//
//  ScreensAssemblerProtocol.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol ScreensAssemblerProtocol: class, NewsModuleAssemblerProtocol {}
