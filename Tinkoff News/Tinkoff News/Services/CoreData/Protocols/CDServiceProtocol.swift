//
//  CDServiceProtocol.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreData

/// Protocol for wrap CD work
protocol CDServiceProtocol: CDServiceProtocolNews {
    
    /// Container
    var persistentContainer: NSPersistentContainer { get }
    
    /// Save context
    ///
    /// - Parameter context: context
    func save(context: NSManagedObjectContext)
    
}
