//
//  Date+Milliseconds.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 04/06/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

extension Date {
    
    /// Create date from milliseconds
    ///
    /// - Parameter milliseconds: milliseconds
    init(milliseconds: TimeInterval) {
        self.init(timeIntervalSince1970: milliseconds / 1000)
    }
    
}
