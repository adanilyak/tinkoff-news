//
//  NewsHeaderTableViewCell.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 05/06/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Cell for news header
class NewsHeaderTableViewCell: UITableViewCell {
    
    /// Item for configuring cell
    struct Item {
        let title: String
        let publicationDate: String
        let viewsCount: String
    }
    
    static let reuseIdentifier: String = String(describing: NewsHeaderTableViewCell.self)
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16.0)
        label.textColor = .black
        return label
    }()
    
    private let publicationDateLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = .lightGray
        return label
    }()
    
    private let viewsCountLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = .lightGray
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(publicationDateLabel)
        contentView.addSubview(viewsCountLabel)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        publicationDateLabel.translatesAutoresizingMaskIntoConstraints = false
        viewsCountLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let tlLeft = NSLayoutConstraint(item: titleLabel,
                                        attribute: .leading,
                                        relatedBy: .equal,
                                        toItem: contentView,
                                        attribute: .leading,
                                        multiplier: 1.0,
                                        constant: 16.0)
        let tlRight = NSLayoutConstraint(item: titleLabel,
                                         attribute: .trailing,
                                         relatedBy: .equal,
                                         toItem: contentView,
                                         attribute: .trailing,
                                         multiplier: 1.0,
                                         constant: -16.0)
        let tlTop = NSLayoutConstraint(item: titleLabel,
                                       attribute: .top,
                                       relatedBy: .equal,
                                       toItem: contentView,
                                       attribute: .top,
                                       multiplier: 1.0,
                                       constant: 16.0)
        
        let plTop = NSLayoutConstraint(item: publicationDateLabel,
                                       attribute: .top,
                                       relatedBy: .equal,
                                       toItem: titleLabel,
                                       attribute: .bottom,
                                       multiplier: 1.0,
                                       constant: 8.0)
        
        let plLeft = NSLayoutConstraint(item: publicationDateLabel,
                                        attribute: .leading,
                                        relatedBy: .equal,
                                        toItem: contentView,
                                        attribute: .leading,
                                        multiplier: 1.0,
                                        constant: 16.0)
        
        let plBottom = NSLayoutConstraint(item: publicationDateLabel,
                                          attribute: .bottom,
                                          relatedBy: .equal,
                                          toItem: contentView,
                                          attribute: .bottom,
                                          multiplier: 1.0,
                                          constant: -16.0)
        
        let vclTop = NSLayoutConstraint(item: viewsCountLabel,
                                        attribute: .top,
                                        relatedBy: .equal,
                                        toItem: titleLabel,
                                        attribute: .bottom,
                                        multiplier: 1.0,
                                        constant: 8.0)
        
        let vclRight = NSLayoutConstraint(item: viewsCountLabel,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: contentView,
                                          attribute: .trailing,
                                          multiplier: 1.0,
                                          constant: -16.0)
        
        let vclBottom = NSLayoutConstraint(item: viewsCountLabel,
                                           attribute: .bottom,
                                           relatedBy: .equal,
                                           toItem: contentView,
                                           attribute: .bottom,
                                           multiplier: 1.0,
                                           constant: -16.0)
        
        contentView.addConstraints([tlLeft, tlRight, tlTop, plTop, plLeft, plBottom, vclTop, vclRight, vclBottom])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Configure cell with item's data
    ///
    /// - Parameter item: item
    func configure(with item: Item) {
        titleLabel.text = item.title
        publicationDateLabel.text = item.publicationDate
        viewsCountLabel.text = item.viewsCount
    }
    
}
