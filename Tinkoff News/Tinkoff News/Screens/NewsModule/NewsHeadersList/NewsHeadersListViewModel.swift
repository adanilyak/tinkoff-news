//
//  NewsHeadersListViewModel.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 29/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreData
import UIKit.UITableView

/// Protocol for interaction with the VC
protocol NewsHeadersListViewModelProtocol: class {
    
    /// Ask table view to begin updates
    func willChangeContent()
    
    /// Tell table view to end updates
    func didChangeContent()
    
    /// Insert row at indexpath with row animation
    ///
    /// - Parameters:
    ///   - indexPath: indexpath
    ///   - animation: rowanimation
    func insert(at indexPath: IndexPath, animation: UITableViewRowAnimation)
    
    /// Delete row at indexpath with row animation
    ///
    /// - Parameters:
    ///   - indexPath: indexpath
    ///   - animation: rowanimation
    func delete(at indexPath: IndexPath, animation: UITableViewRowAnimation)
    
    /// Update row at indexpath with row animation
    ///
    /// - Parameters:
    ///   - indexPath: indexpath
    ///   - animation: rowanimation
    func update(at indexPath: IndexPath, animation: UITableViewRowAnimation)
    
    /// Reload data in table view
    func reloadData()
    
    /// Hide pull to refresh if needed
    func stopRefreshAnimation()
    
    /// Show alert with activity indicator
    func showOnScreenLoading()
    
    /// Hide alert with activity indicator
    ///
    /// - Parameter completion: completion
    func hideOnScreenLoading(completion: (() -> Void)?)
    
    /// Show alert about not reachable network
    ///
    /// - Parameter completion: completion
    func showOfflineAlert(completion: (() -> Void)?)
    
}

/// Config for initing VM
struct NewsHeadersListViewModelInitConfig {
    let newsFacade: NewsFacade
    let router: NewsModuleRouterProtocol
}

/// VM
@objc class NewsHeadersListViewModel: NSObject {
    
    /// Info about paging state
    struct PageLoadingInfo {
        // Offset from beginning of list, start index for loading
        var offset: Int = 0
        // Page size to load
        let pageSize: UInt = 20
        // Is now loading
        var isLoading: Bool = false
        // Is all headers loaded
        var isAllLoaded: Bool = false
    }
    
    /// Paging info
    private var pageLoadingInfo: PageLoadingInfo = PageLoadingInfo()
    
    private let newsFacade: NewsFacade
    
    private let router: NewsModuleRouterProtocol
    
    weak var vmDelegate: NewsHeadersListViewModelProtocol?
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss, dd/MM/yyyy"
        return formatter
    }()
    
    /// Fetch controller for news headers
    /// Headers are sorted by publication date from new to old
    private(set) lazy var fetchController: NSFetchedResultsController<NewsHeader> = {
        let fetchRequest = NSFetchRequest<NewsHeader>(entityName: CDEntityName.newsHeader.rawValue)
        let pulicationDateDescriptor = NSSortDescriptor(key: "publicationDate", ascending: false)
        fetchRequest.sortDescriptors = [pulicationDateDescriptor]
        let fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                managedObjectContext: newsFacade.coreData.persistentContainer.viewContext,
                                                                sectionNameKeyPath: nil,
                                                                cacheName: nil)
        fetchResultsController.delegate = self
        return fetchResultsController
    }()
    
    init(config: NewsHeadersListViewModelInitConfig) {
        self.newsFacade = config.newsFacade
        self.router = config.router
    }
    
    /// Start loading from the offset 0
    /// Use to update data (dowload new headers if they exist)
    ///
    /// - Parameter isRefresh: is called from pull to refresh
    func startLoadingFromTheBeginning(isRefresh: Bool = false) {
        // Check is network reachable
        guard ReachabilityHelper.isNetworkReachable() else {
            
            // Set content offset to curent number of headers
            // if the network connection established, we will continue loading pages normaly
            if let numberOfObjects = fetchController.sections?.first?.numberOfObjects {
                pageLoadingInfo.offset = numberOfObjects
            }
            
            // If user tries to refresh data while the network is unreachable
            // He will get an alert
            if isRefresh {
                vmDelegate?.showOfflineAlert() { [weak self] in
                    self?.vmDelegate?.stopRefreshAnimation()
                }
            }
            
            return
        }
        
        pageLoadingInfo.offset = 0
        pageLoadingInfo.isAllLoaded = false
        loadNextPage()
    }
    
    /// Load next page according to page info
    private func loadNextPage() {
        guard !pageLoadingInfo.isLoading,
            !pageLoadingInfo.isAllLoaded,
            ReachabilityHelper.isNetworkReachable() else {
                vmDelegate?.stopRefreshAnimation()
                return
        }
        
        pageLoadingInfo.isLoading = true
        newsFacade.loadNewsHeaders(for: UInt(pageLoadingInfo.offset),
                                   pageSize: pageLoadingInfo.pageSize) { [weak self] result in
            self?.pageLoadingInfo.isLoading = false
            DispatchQueue.main.async { [weak self] in
                self?.vmDelegate?.stopRefreshAnimation()
            }
            
            switch result {
            case .error(let error):
                print(error)
            case .success(let headers, let hasIntersections):
                // In case of seccess loading
                // If headers are empty -> All data has been loaded
                if headers.count == 0 {
                    self?.pageLoadingInfo.isAllLoaded = true
                }
                
                // If while loading the page we noticed
                // that loaded headers have intersections with out data
                // It means that we loaded all necessary headers from the top
                // And we can continue to load data from the bottom
                if hasIntersections,
                    let totalObjects = self?.fetchController.sections?.first?.numberOfObjects {
                    self?.pageLoadingInfo.offset = totalObjects
                } else {
                    self?.pageLoadingInfo.offset += headers.count
                }
            }
        }
    }
    
    /// Fetch data using fetch result controller
    /// and reload table view
    func fetchAndReload() {
        try? fetchController.performFetch()
        vmDelegate?.reloadData()
    }
    
    /// Check if we need to load data when
    /// cell at index path is going to be displayed
    /// And load if needed
    ///
    /// - Parameter indexPath: indexpath
    func loadNextPageIfNeededWhenWillDisplay(indexPath: IndexPath) {
        guard indexPath.section == 0,
            max(pageLoadingInfo.offset - 1, 0) == indexPath.row else {
                return
        }
        
        loadNextPage()
    }
    
    /// Action on tap at indexpath
    ///
    /// - Parameter indexPath: indexpath
    func onSelect(at indexPath: IndexPath) {
        let object: NewsHeader = fetchController.object(at: indexPath)
        // If content is alreay loaded, no need to load it second time
        if let content = object.content {
            routeToContent(with: content)
        } else {
            loadContentAndShow(for: object)
        }
    }
    
    /// Load content and route to content VC
    ///
    /// - Parameter header: header of content
    func loadContentAndShow(for header: NewsHeader) {
        guard ReachabilityHelper.isNetworkReachable() else {
            vmDelegate?.showOfflineAlert(completion: nil)
            return
        }
        
        vmDelegate?.showOnScreenLoading()
        newsFacade.loadNewsContent(for: header) { [weak self] result in
            switch result {
            case .error(let error):
                DispatchQueue.main.async { [weak self] in
                    self?.vmDelegate?.hideOnScreenLoading(completion: nil)
                }
                print(error)
                break
            case .success(let content):
                DispatchQueue.main.async { [weak self] in
                    self?.vmDelegate?.hideOnScreenLoading() { [weak self] in
                        self?.routeToContent(with: content)
                    }
                }
            }
        }
    }
    
    /// Route to content VC
    /// Also increment the view counter by 1 and save result
    ///
    /// - Parameter content: content
    private func routeToContent(with content: NewsContent) {
        router.routeToNewsContent(with: content)
        
        if let header = content.header {
            newsFacade.increaseViewsCount(for: header)
        }
    }
    
    /// Convert model to item of header cell
    ///
    /// - Parameter header: header
    /// - Returns: item
    func createItem(from header: NewsHeader) -> NewsHeaderTableViewCell.Item {
        let dateString: String
        if let date = header.publicationDate {
            dateString = dateFormatter.string(from: date)
        } else {
            dateString = ""
        }
        return NewsHeaderTableViewCell.Item(title: header.text ?? tr(key: "news_header_list.header_cell.empty_title"),
                                            publicationDate: dateString,
                                            viewsCount: header.numberOfViews > 0 ? "\(header.numberOfViews)" : "")
    }
    
}

// MARK: - NSFetchedResultsControllerDelegate
extension NewsHeadersListViewModel: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        vmDelegate?.willChangeContent()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        vmDelegate?.didChangeContent()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            guard let indexPath = indexPath else { return }
            vmDelegate?.delete(at: indexPath, animation: .fade)
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            vmDelegate?.insert(at: newIndexPath, animation: .fade)
        case .move:
            guard let indexPath = indexPath,
                let newIndexPath = newIndexPath else { return }
            vmDelegate?.delete(at: indexPath, animation: .fade)
            vmDelegate?.insert(at: newIndexPath, animation: .fade)
        case .update:
            guard let indexPath = indexPath else { return }
            vmDelegate?.update(at: indexPath, animation: .none)
        }
    }
    
}
