//
//  CDService+News.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 28/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import CoreData

// MARK: - CDServiceProtocolNews
extension CDService: CDServiceProtocolNews {
    
    func createNewsHeader(from json: JSON, context: NSManagedObjectContext) -> NewsHeader? {
        guard let id: Int64 = Int64(with: json["id"] as? String),
            let bankTypeId: Int64 = json["bankInfoTypeId"] as? Int64 else {
                assert(false, "Check that all non-optional fileds exist to successfully parse the model")
                return nil
        }
        
        let header: NewsHeader
        
        let request = NSFetchRequest<NewsHeader>(entityName: CDEntityName.newsHeader.rawValue)
        request.predicate = NSPredicate(format: "id == %@", "\(id)")
        request.fetchLimit = 1
        if let object = (try? context.fetch(request))?.first {
            header = object
        } else if let object = NSEntityDescription.insertNewObject(forEntityName: CDEntityName.newsHeader.rawValue, into: context) as? NewsHeader {
            header = object
        } else {
            return nil
        }
        
        header.id = id
        header.bankInfoTypeId = bankTypeId
        header.name = json["name"] as? String
        header.text = json["text"] as? String
        
        if let publicationDateJson = json["publicationDate"] as? JSON,
            let milliseconds = publicationDateJson["milliseconds"] as? TimeInterval {
            header.publicationDate = Date(milliseconds: milliseconds)
        }
        
        return header
    }
    
    func createNewsContent(from json: JSON, context: NSManagedObjectContext) -> NewsContent? {
        guard let bankTypeId: Int64 = json["bankInfoTypeId"] as? Int64 else {
            assert(false, "Check that all non-optional fileds exist to successfully parse the model")
            return nil
        }
        
        guard let content = NSEntityDescription.insertNewObject(forEntityName: CDEntityName.newsContent.rawValue, into: context) as? NewsContent else {
            return nil
        }
        
        content.bankInfoTypeId = bankTypeId
        content.content = json["content"] as? String
        content.typeId = json["typeId"] as? String
        
        if let creationDateJson = json["creationDate"] as? JSON,
            let milliseconds = creationDateJson["milliseconds"] as? TimeInterval {
            content.creationDate = Date(milliseconds: milliseconds)
        }
        
        if let lastModificationDateJson = json["lastModificationDate"] as? JSON,
            let milliseconds = lastModificationDateJson["milliseconds"] as? TimeInterval {
            content.lastModificationDate = Date(milliseconds: milliseconds)
        }
        
        return content
    }
    
    func isNewsHeaderExistWithId(in ids: [Int64], context: NSManagedObjectContext) -> Bool {
        let request = NSFetchRequest<NewsHeader>(entityName: CDEntityName.newsHeader.rawValue)
        request.predicate = NSPredicate(format: "id IN %@", ids)
        request.fetchLimit = 1
        
        guard let results = try? context.fetch(request) else {
            return false
        }
        
        return !results.isEmpty
    }
    
}
