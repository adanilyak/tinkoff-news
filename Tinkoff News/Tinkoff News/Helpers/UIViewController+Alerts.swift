//
//  UIViewController+Alerts.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 06/06/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Show alert wrapper
    ///
    /// - Parameters:
    ///   - title: title
    ///   - message: message
    ///   - actions: actions
    ///   - completion: completion
    func showAlert(title: String? = nil,
                   message: String? = nil,
                   actions: [UIAlertAction],
                   completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { alert.addAction($0) }
        present(alert, animated: true, completion: completion)
    }
    
    /// Shortcut for showing alert about unavailable network
    ///
    /// - Parameter completion: completion
    func showNoNetworkAlert(completion: (() -> Void)? = nil) {
        showAlert(title: tr(key: "alert.network.title"),
                  message: tr(key: "alert.network.message"),
                  actions: [UIAlertAction(title: tr(key: "alert.network.cancel"),
                                          style: .cancel,
                                          handler: nil)],
                  completion: completion)
    }
    
    /// Show loading alert
    ///
    /// - Parameters:
    ///   - title: title, if not specified used default value
    ///   - completion: completion
    /// - Returns: alert controller
    func showLoadingAlert(title: String? = nil,
                          completion: (() -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title ?? tr(key: "alert.activity.title"),
                                      message: nil,
                                      preferredStyle: .alert)
        
        let indicator = UIActivityIndicatorView(frame: alert.view.bounds)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.addConstraints([NSLayoutConstraint(item: indicator,
                                                     attribute: .height,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1.0,
                                                     constant: 40.0),
                                  NSLayoutConstraint(item: indicator,
                                                     attribute: .width,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1.0,
                                                     constant: 40.0)])
        indicator.activityIndicatorViewStyle = .gray

        alert.view.addSubview(indicator)
        alert.view.addConstraints([NSLayoutConstraint(item: alert.view,
                                                      attribute: .height,
                                                      relatedBy: .equal,
                                                      toItem: nil,
                                                      attribute: .notAnAttribute,
                                                      multiplier: 1.0,
                                                      constant: 100.0),
                                   NSLayoutConstraint(item: indicator,
                                                      attribute: .bottom,
                                                      relatedBy: .equal,
                                                      toItem: alert.view,
                                                      attribute: .bottom,
                                                      multiplier: 1.0,
                                                      constant: -16.0),
                                   NSLayoutConstraint(item: indicator,
                                                      attribute: .centerX,
                                                      relatedBy: .equal,
                                                      toItem: alert.view,
                                                      attribute: .centerX,
                                                      multiplier: 1.0,
                                                      constant: 0.0)])
        indicator.isUserInteractionEnabled = false
        indicator.startAnimating()
        
        present(alert, animated: true, completion: completion)
        
        return alert
    }
    
}
