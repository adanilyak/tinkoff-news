//
//  NewsHeadersListViewController.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 27/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Controller for displaying the list of headers
/// Allows to load headers by pages
/// Allows to see the content offline
class NewsHeadersListViewController: UIViewController {
    
    /// VM
    private let viewModel: NewsHeadersListViewModel
    
    /// Table for headers
    private lazy var tableView: UITableView = UITableView()
    
    /// Temp property for loading alert
    /// Used to dismiss the alert then it no longer needed
    private var currentlyShowingAlert: UIAlertController?
    
    init(_ viewModel: NewsHeadersListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.vmDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
        
        title = tr(key: "news_header_list.title")
        
        view.backgroundColor = UIColor.green
        
        // Fetching data from DB for offline display
        viewModel.fetchAndReload()
        
        // Trying to load updated data from the server
        viewModel.startLoadingFromTheBeginning()
    }
    
    private func tableViewSetup() {
        view.addSubview(tableView)
        
        tableView.register(NewsHeaderTableViewCell.self,
                           forCellReuseIdentifier: NewsHeaderTableViewCell.reuseIdentifier)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80.0
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let left = NSLayoutConstraint(item: tableView,
                                      attribute: .leading,
                                      relatedBy: .equal,
                                      toItem: view,
                                      attribute: .leading,
                                      multiplier: 1.0,
                                      constant: 0.0)
        let right = NSLayoutConstraint(item: tableView,
                                       attribute: .trailing,
                                       relatedBy: .equal,
                                       toItem: view,
                                       attribute: .trailing,
                                       multiplier: 1.0,
                                       constant: 0.0)
        let top = NSLayoutConstraint(item: tableView,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: view,
                                     attribute: .top,
                                     multiplier: 1.0,
                                     constant: 0.0)
        let bottom = NSLayoutConstraint(item: tableView,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: view,
                                        attribute: .bottom,
                                        multiplier: 1.0,
                                        constant: 0.0)
        view.addConstraints([left, right, top, bottom])
        
        tableView.delegate = self.viewModel
        tableView.dataSource = self.viewModel
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self,
                                            action: #selector(onRefresh),
                                            for: .valueChanged)
    }
    
}

// MARK: - Actions
extension NewsHeadersListViewController {
    
    @objc private func onRefresh() {
        viewModel.startLoadingFromTheBeginning(isRefresh: true)
    }
    
}

// MARK: - NewsHeadersListViewModelProtocol
extension NewsHeadersListViewController: NewsHeadersListViewModelProtocol {
    
    func willChangeContent() {
        tableView.beginUpdates()
    }
    
    func didChangeContent() {
        tableView.endUpdates()
    }
    
    func insert(at indexPath: IndexPath, animation: UITableViewRowAnimation) {
        tableView.insertRows(at: [indexPath], with: animation)
    }
    
    func delete(at indexPath: IndexPath, animation: UITableViewRowAnimation) {
        tableView.deleteRows(at: [indexPath], with: animation)
    }
    
    func update(at indexPath: IndexPath, animation: UITableViewRowAnimation) {
        tableView.reloadRows(at: [indexPath], with: animation)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func stopRefreshAnimation() {
        tableView.refreshControl?.endRefreshing()
    }
    
    func showOnScreenLoading() {
        currentlyShowingAlert = showLoadingAlert()
    }
    
    func hideOnScreenLoading(completion: (() -> Void)?) {
        currentlyShowingAlert?.dismiss(animated: true, completion: completion)
        currentlyShowingAlert = nil
    }
    
    func showOfflineAlert(completion: (() -> Void)?) {
        showNoNetworkAlert(completion: completion)
    }
    
}
