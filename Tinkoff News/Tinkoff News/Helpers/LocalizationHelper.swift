//
//  LocalizationHelper.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 02/06/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Shortcut for getting localized string
///
/// - Parameter key: key
/// - Returns: localized string
func tr(key: String) -> String {
    return NSLocalizedString(key, comment: "")
}
