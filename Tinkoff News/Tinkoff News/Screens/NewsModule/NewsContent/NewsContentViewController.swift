//
//  NewsContentViewController.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 04/06/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// View Controller for displaying news' content
class NewsContentViewController: UIViewController {
    
    /// VM
    private let viewModel: NewsContentViewModel
    
    /// Web View for showing downloaded HTML string
    private let contentWebView: UIWebView = UIWebView()
    
    init(_ viewModel: NewsContentViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        contentWebView.loadHTMLString(viewModel.htmlString, baseURL: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.title
        setupWebContentView()
    }
    
    private func setupWebContentView() {
        view.addSubview(contentWebView)
        
        contentWebView.backgroundColor = UIColor.white
        
        contentWebView.translatesAutoresizingMaskIntoConstraints = false
        let left = NSLayoutConstraint(item: contentWebView,
                                      attribute: .leading,
                                      relatedBy: .equal,
                                      toItem: view,
                                      attribute: .leading,
                                      multiplier: 1.0,
                                      constant: 0.0)
        let right = NSLayoutConstraint(item: contentWebView,
                                       attribute: .trailing,
                                       relatedBy: .equal,
                                       toItem: view,
                                       attribute: .trailing,
                                       multiplier: 1.0,
                                       constant: 0.0)
        let top = NSLayoutConstraint(item: contentWebView,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: view,
                                     attribute: .top,
                                     multiplier: 1.0,
                                     constant: 0.0)
        let bottom = NSLayoutConstraint(item: contentWebView,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: view,
                                        attribute: .bottom,
                                        multiplier: 1.0,
                                        constant: 0.0)
        view.addConstraints([left, right, top, bottom])
    }
    
}
