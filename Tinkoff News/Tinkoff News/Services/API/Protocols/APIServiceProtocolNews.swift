//
//  APIServiceProtocolNews.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 27/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// API protocol required for News User Stoty
protocol APIServiceProtocolNews {
    
    /// Get news headers from the server with offset and page size
    ///
    /// - Parameters:
    ///   - offset: offset, start index
    ///   - pageSize: page size
    ///   - completion: completion with result
    func getNewsHeaders(for offset: UInt, pageSize: UInt, completion: APICompletion<JSON>?)
    
    /// Get news content from the server
    ///
    /// - Parameters:
    ///   - id: id of content
    ///   - completion: completion with result
    func getNewsContent(for id: Int, completion: APICompletion<JSON>?)
    
}
