//
//  NewsHeadersListViewModel+TableView.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 29/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

// MARK: - UITableViewDelegate
extension NewsHeadersListViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        loadNextPageIfNeededWhenWillDisplay(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        onSelect(at: indexPath)
    }
    
}

// MARK: - UITableViewDataSource
extension NewsHeadersListViewModel: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchController.sections?.first?.numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: NewsHeaderTableViewCell
            = tableView.dequeueReusableCell(withIdentifier: NewsHeaderTableViewCell.reuseIdentifier, for: indexPath) as? NewsHeaderTableViewCell else {
                assert(false, "Unknown cell type")
                return UITableViewCell()
        }
        
        let header = fetchController.object(at: indexPath)
        cell.configure(with: createItem(from: header))
        
        return cell
    }
    
}
