//
//  AppDelegate.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 27/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /// Assembler for building all possible application screens
    private var assembler: ScreensAssembler?
    
    /// Main application router
    private var router: Router?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        assembler = ScreensAssembler()
        
        guard let assembler: ScreensAssembler = assembler else {
            assert(false, "Assembler must exist")
            return false
        }
        
        router = Router(with: assembler)
        router?.routeToNewsHeadersList()
        
        guard let router: Router = router else {
            assert(false, "Router must exist")
            return false
        }
        
        window = createWindow(with: router.rootController)
        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        guard let assembler = assembler else { return }
        let context = assembler.newsFacade.coreData.persistentContainer.viewContext
        assembler.newsFacade.coreData.save(context: context)
    }

}

// MARK: - AppDelegate + Windwow
extension AppDelegate {
    
    /// Create main application window
    ///
    /// - Parameter rootController: root controller
    /// - Returns: window
    private func createWindow(with rootController: UIViewController) -> UIWindow {
        let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.white
        window.rootViewController = rootController
        return window
    }
    
}

