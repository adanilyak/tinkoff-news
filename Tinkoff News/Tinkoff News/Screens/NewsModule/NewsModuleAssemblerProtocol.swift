//
//  NewsModuleAssemblerProtocol.swift
//  Tinkoff News
//
//  Created by Alexander Danilyak on 29/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import UIKit.UIViewController

/// Assembler protocol needed for News User Story
protocol NewsModuleAssemblerProtocol {
    
    /// Assemble headers list VC
    ///
    /// - Parameter router: router
    /// - Returns: VC
    func assembleNewsHeadersListViewController(router: NewsModuleRouterProtocol) -> UIViewController
    
    /// Assemble content VC
    ///
    /// - Parameter content: content
    /// - Returns: VC
    func assembleNewsContentViewController(with content: NewsContent) -> UIViewController
    
}
